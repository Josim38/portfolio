<?php include'inc/include.php'; ?>

<!DOCTYPE html>
<html lang="fr"><!-- InstanceBegin template="/Templates/_modele.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Portfolio</title>
<!-- InstanceEndEditable -->
	<meta charset="utf-8">
	<link href='http://fonts.googleapis.com/css?family=Courgette' rel='stylesheet' type='text/css'/>
    <link href='http://fonts.googleapis.com/css?family=Quando' rel='stylesheet' type='text/css'/>
	<link href="css/style.css" type="text/css" rel="stylesheet" />
	<link href="favicon.ico"  rel="shortcut icon" type="image/x-icon" />
    <script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/dynamique.js"></script>
    <script type="text/javascript" src="js/jquery.bxSlider.js"></script>
    <!-- InstanceBeginEditable name="head" --><!-- InstanceEndEditable -->
    <script type="text/javascript">
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
    </script>
</head>
<body>
<img id=logo_ciel src="gifs/logo.png">
<div id="header_frame">
            <div id="header">
              
			<ul id="menu">
				<li class="element1"><a <?php if($url_en_cours=="index.php") echo "class='en-cours'"; ?> href="index.php">Accueil</a></li>
				<li class="element2"><a <?php if($url_en_cours=="portfolio.php") echo "class='en-cours'"; ?>href="portfolio.php">Portfolio</a></li>
				<li class="element3"><a <?php if($url_en_cours=="cv.php") echo "class='en-cours'"; ?>href="cv.php">CV</a></li>
			</ul>
            	
  </div>
</div>

<div id="content">
<!-- InstanceBeginEditable name="content" -->
<div id="colonnea">
<ul>
	<li>Jordan Andrevon, 27 Ans</li>
	<li>Le Mollard 38120 Proveyzieux</li>
	<li> 07.77.07.68.55.</li>
	<li>j.andrevon@orange.fr</li>
	<li>Loisirs : musique, graphisme, univers du web, jeux vidéos, cinéma...</li>
</ul>
</div> 
<h1 id="titrexp">Expériences</h1>
<div id="colonneb">
<ul>
	<li> Juin 2012 : Stage : Osis web Réalisation de la charte graphique du site Mumble infinity</li>
	<li> Juin 2013 : Réalisation de la charte graphique et du logo de l'espace de coworking : Korwork (Kowork.fr)</li>
	<li> 2013 : Réalisation de mon portfolio avec des chartes graphiques fictives et autres projets réalisés en formation.</li>
	<li> Février 2014 : Réalisation du logo et conception d'un flyers pour la société de traduction "ALMA traduction".</li>
</ul>
</div>

<h1 id="titreform">Formations/Compétences</h1>
<div id="colonnec">
<ul>
	<li>2017 : Formation Developpement web (HTML5, CSS3, PHP, JS) </li>
	<li>2014-2015 : Formation Infographiste Metteur en page ADAPT de Troyes</li>
	<li>Photoshop (chromie, détourage, montage photos)</li>
	<li>Illustrator (logos, compositions, éléments)</li>
	<li>Indesign (Mise en page)</li>
</ul>
</div>

<div id="poneya">
<img src="cvimg/images/poneygauche.png" alt="poneya" />
</div>

<div id="poneyb">
<img src="cvimg/images/poneydroite.png" alt="poneyb" />
</div>

<!-- InstanceEndEditable -->

</div>
		<div id="footer_frame">
		  <div id="footer">
          <a href="mailto:j.andrevon@orange.fr" id="banderolle"></a>
	
		  </div>
          </div>
</body>
<!-- InstanceEnd --></html>