<?php include'inc/include.php'; ?>

<!DOCTYPE html>
<html lang="fr"><!-- InstanceBegin template="/Templates/_modele.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Portfolio</title>
<!-- InstanceEndEditable -->
	<meta charset="utf-8">
	<link href='http://fonts.googleapis.com/css?family=Courgette' rel='stylesheet' type='text/css'/>
    <link href='http://fonts.googleapis.com/css?family=Quando' rel='stylesheet' type='text/css'/>
	<link href="css/style.css" type="text/css" rel="stylesheet" />
	<link href="favicon.ico"  rel="shortcut icon" type="image/x-icon" />
    <script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/dynamique.js"></script>
    <script type="text/javascript" src="js/jquery.bxSlider.js"></script>
    <!-- InstanceBeginEditable name="head" -->
  
				<!-- InstanceEndEditable -->
    <script type="text/javascript">
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
    </script>
</head>
<body>
<img id=logo_ciel src="gifs/logo.png">
<div id="header_frame">
            <div id="header">
              
			<ul id="menu">
				<li class="element1"><a <?php if($url_en_cours=="index.php") echo "class='en-cours'"; ?> href="index.php">Accueil</a></li>
				<li class="element2"><a <?php if($url_en_cours=="portfolio.php") echo "class='en-cours'"; ?>href="portfolio.php">Portfolio</a></li>
				<li class="element3"><a <?php if($url_en_cours=="cv.php") echo "class='en-cours'"; ?>href="cv.php">CV</a></li>
			</ul>
            	
  </div>
</div>

<div id="content">
<!-- InstanceBeginEditable name="content" -->
<div><a id="twitter" href="http://twitter.com/parci_" target="_blank"></a></div>
<div><a id="facebook" href="https://www.linkedin.com/in/jordan-andrevon-53a4009a/" target="_blank"></a></div>
<div id="fb"><img src="gifs/elipse_fb.png" alt="elipse" /></div>
<div id="tw"><img src="gifs/elipse_fb.png" alt="elipsetw"/></div>
<div id="carrousel_frame">
	<ul id="slider1">
		<li>
			<img src="imgcarou/standardiste_carrou.jpg" alt="standardiste" />
			<div class="clear"></div>
		</li>
		<li>
       <img src="imgcarou/voiture_carrou.jpg" alt="voiture" />
			<div class="clear"></div>
		</li>
        <li>
        <img src="imgcarou/cerf_saison.jpg" alt="cerf"/>
        <div class="clear"></div>
        </li>
	</ul>
</div>

<div id="text_accueil">
<p>Bienvenue sur mon portfolio</p>
</div>
<div id="text_presentation">
<p>
Bonjour. Je m'appelle Jordan Andrevon et bienvenue dans mon univers ! Je vous invite à découvrir mes réalisations effectuées lors de mes différentes expériences professionnelles.
</p></div>

<!-- InstanceEndEditable -->

</div>
		<div id="footer_frame">
		  <div id="footer">
          <a href="mailto:j.andrevon@orange.fr" id="banderolle"></a>
	
		  </div>
          </div>
</body>
<!-- InstanceEnd --></html>