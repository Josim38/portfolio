<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
<head>
	<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
	<title>PicsEngine Server Compatibility Test</title>
	
	<style type="text/css" media="screen">
		html, body {
			height: 100%;
		}
		body {
			background: #131313;
			font: 14px "Lucida Grande", "Trebuchet MS", Verdana, sans-serif;
		}
		body, h1, h2, h3, h4, h5, p, ul, li, img, form, dl, dd, dt {
			margin: 0;
			padding: 0;
			border: 0;
			font-weight: normal;
			font-family: "Lucida Grande", "Trebuchet MS", Verdana, sans-serif;
		}
		a {
			color: #fff;
			font-weight: bold;
		}
		
		#englobe {
			background: #000;
			margin: 20px auto 20px auto;
			padding: 15px;
			color: #fff;
			width: 500px;
		}
		h1 {
			font-size: 20px;
			padding-bottom: 10px;
		}
		.test {
			border-bottom: 1px solid #131313;
			padding: 4px;
			overflow: hidden;
		}
		.test p {
			float: right;
			font-size: 12px;
		}
		.test h2 {
			float: left;
			font-size: 12px;
		}
		.test.fail {
			background-color: #af1616;
			color: #fff;
		}
		.test.fail p {
			padding-top: 5px;
			float: left;
		}
		
		.result {
			font-size: 12px;
			padding: 15px 4px 4px 4px;
		}
	</style>
</head>
<body>
	<div id="englobe">
		<h1>PicsEngine Server Compatibility Test</h1>
		<?php
		$tests = Array(
			Array(
				'title'		=>	'PHP5',
				'test'		=>	(version_compare(PHP_VERSION, '5', '>')),
				'fail'		=>	'Your server hasn\'t PHP5 installed. Consult your host to upgrade PHP.',
				'important'	=>	true,
				),
			Array(
				'title'		=>	'MySQL',
				'test'		=>	(extension_loaded('mysql')),
				'fail'		=>	'Your server hasn\'t MySQL installed. Consult your host to have MySQL.',
				'important'	=>	true,
				),
			Array(
				'title'		=>	'File upload',
				'test'		=>	(ini_get('file_uploads')),
				'fail'		=>	'Your PHP configuration refuse file uploads.',
				'important'	=>	true,
				),
			Array(
				'title'		=>	'GD libraries',
				'test'		=>	(extension_loaded('gd') && function_exists('gd_info')),
				'fail'		=>	'GD is not installed on your server. Consult your host to install GD.',
				'important'	=>	true,
				),
			);
		
		$picsengine = true;
		foreach($tests as $test) {
			if(!$test['test'] && $test['important'])
				$picsengine = false;
			?>
			<div class="test<?php if(!$test['test']): echo ' fail'; endif; ?>">
				<h2><?php echo $test['title']; ?></h2>
				<?php if(!$test['test']): ?>
					<p><?php echo $test['fail']; ?></p>
				<?php else: ?>
					<p>OK</p>
				<?php endif; ?>
			</div>
			<?php
		}
	
		if(!$picsengine) {
			?>
			<div class="result">PicsEngine cannot be installed.</div>
			<?php
		}
		else {
			?>
			<div class="result">PicsEngine can be installed. <a href="http://www.picsengine.com/">Download it</a>.</div>
			<?php
		}
		?>
	</div>
</body>
</html>