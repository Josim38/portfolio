<?php include'inc/include.php'; ?>

<!DOCTYPE html>
<html lang="fr"><!-- InstanceBegin template="/Templates/_modele.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Portfolio</title>
<!-- InstanceEndEditable -->
	<meta charset="utf-8">
	<link href='http://fonts.googleapis.com/css?family=Courgette' rel='stylesheet' type='text/css'/>
    <link href='http://fonts.googleapis.com/css?family=Quando' rel='stylesheet' type='text/css'/>
	<link href="css/style.css" type="text/css" rel="stylesheet" />
	<link href="favicon.ico"  rel="shortcut icon" type="image/x-icon" />
    <script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/dynamique.js"></script>
    <script type="text/javascript" src="js/jquery.bxSlider.js"></script>
    <!-- InstanceBeginEditable name="head" -->
	  <p id="ref">Phrase de référencement</p>
	  

       
			<h1>Page de <strong>redirection</strong></h1>
			
			<h2>L' URL <strong>n'est pas valide</strong> </h2>
			
			<p>La page que vous essayez de consulter n'est pas/plus une page valide de notre site.
			<br /><br />
			Cette erreur a pu se produire pour une des raisons suivantes :</p>
			
			<div class="fakeUl">
				<p class="fakeLi"><strong>L'URL</strong> que vous avez saisie <strong>contient une erreur.</strong></p>
				<p class="fakeLi"><strong>La page</strong> que vous essayez de consulter <strong>a été retirée de notre site.</strong></p>
				<p class="fakeLi">Vous avez cliqué sur <strong>un lien mort.</strong><br />
				Dans ce cas, merci de nous prévenir par le biais de <a href="contact.php">notre formulaire de contact.</a></p>
			</div>

	   
	    
	<!-- InstanceEndEditable -->
    <script type="text/javascript">
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
    </script>
</head>
<body>
<img id=logo_ciel src="gifs/logo.png">
<div id="header_frame">
            <div id="header">
              
			<ul id="menu">
				<li class="element1"><a <?php if($url_en_cours=="index.php") echo "class='en-cours'"; ?> href="index.php">Accueil</a></li>
				<li class="element2"><a <?php if($url_en_cours=="portfolio.php") echo "class='en-cours'"; ?>href="portfolio.php">Portfolio</a></li>
				<li class="element3"><a <?php if($url_en_cours=="cv.php") echo "class='en-cours'"; ?>href="cv.php">CV</a></li>
			</ul>
            	
  </div>
</div>

<div id="content">
<!-- InstanceBeginEditable name="content" --><!-- InstanceEndEditable -->

</div>
		<div id="footer_frame">
		  <div id="footer">
          <a href="mailto:j.andrevon@orange.fr" id="banderolle"></a>
	
		  </div>
          </div>
</body>
<!-- InstanceEnd --></html>